! ------------------------------------------------------------------------------
!  DUSTDIFF_SPLIT  This routine solves the dust flux after the operator splitting  
!              
!
!  inputs/outputs
!  uin         => (const)  input state
!  iu1,iu2     => (const)  first and last index of input array,
!  ju1,ju2     => (const)  cell centered,    
!  ku1,ku2     => (const)  including buffer cells.
!  flux       <=  (modify) return fluxes in the 3 coord directions
!  if1,if2     => (const)  first and last index of output array,
!  jf1,jf2     => (const)  edge centered,
!  kf1,kf2     => (const)  for active cells only.
!  dx,dy,dz    => (const)  (dx,dy,dz)
!  dt          => (const)  time step
!  ngrid       => (const)  number of sub-grids
!  ndim        => (const)  number of dimensions
!
!  uin = (\rho eps_1, .... \rho eps_ndust, tstop1, tstopndust,\rho,P)

!
!  This routine was adapted from Yohan Dubois & Benoit Commerçon's
!  heat conduction routine by Ugo Lebreuilly
! --------------------------------------------------------------------------
subroutine dustdiffus(uin,flux,dx,dy,dz,dt,ngrid,fdx)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none
  
  integer ::ngrid
  real(dp)::dx,dy,dz,dt

  ! Input states
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::qin
  real(dp),dimension(1:nvector,iu1:iu2+1,ju1:ju2+1,ku1:ku2+1,1:3)::bf
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndim)::gravin 

  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::fdx

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:ndust,1:ndim)::flux
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:ndust)::Xflux,Yflux,Zflux

  ! Local scalar variables
  integer::i,j,k,l,ivar, idust
  integer::ilo,ihi,jlo,jhi,klo,khi

  ilo=MIN(1,iu1+2); ihi=MAX(1,iu2-2)
  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)

  flux=0.0d0
  Xflux=0.0d0
  Yflux=0.0d0
  Zflux=0.0d0


  call ctoprimdust(uin,qin,bf,gravin,dt,ngrid)
  
 ! Compute the dust flux in X direction
  call dustXflx(qin,Xflux,dx,dt,ngrid,fdx)

  do k=klo,khi
  do j=jlo,jhi
  do i=if1,if2
     do l = 1, ngrid
        do idust = 1, ndust
           flux(l,i,j,k,idust,1)=Xflux(l,i,j,k,idust)
        end do
    enddo
  enddo
  enddo
  enddo
#if NDIM>1
  ! Compute dust flux in Y direction
  call dustYflx(qin,Yflux,dy,dt,ngrid,fdx)
  do k=klo,khi
  do j=jf1,jf2
  do i=ilo,ihi
     do l = 1, ngrid
        do idust = 1, ndust         
           flux(l,i,j,k,idust,2)=Yflux(l,i,j,k,idust)
        end do
     enddo
  enddo
  enddo
  enddo
#endif
  
#if NDIM>2
  ! Compute the dust flux in Z direction
  call dustZflx(qin,Zflux,dz,dt,ngrid,fdx)
  do k=kf1,kf2
  do j=jlo,jhi
  do i=ilo,ihi
     do l = 1, ngrid
        do idust = 1, ndust         
          flux(l,i,j,k,idust,3)=Zflux(l,i,j,k,idust)
        end do
     enddo
  enddo
  enddo
  enddo
#endif




end subroutine dustdiffus

subroutine dustXflx(uin,myflux,dx,dt,ngrid,ffdx)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none 

  integer ::ngrid
  real(dp)::dx,dt
  real(dp), dimension(1:ndust) ::d_grain,l_grain,isnot_charged

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:ndust)::myflux

  ! Primitive variables
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::ffdx

  real(dp)::dx_loc,sum_dust,Tksleft_tot,Tksright_tot,csleft,csright
  real(dp),dimension(1:ndust)::fdust, Tksleft, Tksright
  real(dp),dimension(1:ndust)::fx
  real(dp) :: speed, sigma,dPdx,scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2
  real(dp):: t_dyn,entho,pi
  integer::i,j,k,l,isl,idust, idens, ipress
  integer::jlo,jhi,klo,khi,ilo,ihi
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  entho= one /(gamma -one)
  pi =3.14159265358979323846_dp
  if(mrn.eqv..true.) then
     isnot_charged=0.0d0    
     call size_dust(l_grain)
     do idust=1,ndust
       l_grain(idust) = l_grain(idust)/scale_l
       d_grain(idust)=grain_dens(idust)/scale_d
       isnot_charged(idust)=0.0d0
    end do
  else
     do idust=1,ndust
       d_grain(idust)=grain_dens(idust)/scale_d
       l_grain(idust)=grain_size(idust)/scale_l
       isnot_charged(idust)=0.0d0

    end do
 endif
  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)
  idens=1
  ipress=5
  do k=klo,khi
  do j=jlo,jhi
  do i=if1,if2
     do l = 1, ngrid
        Tksleft     = 0.0_dp
        Tksright    = 0.0_dp
        Tksleft_tot = 0.0_dp
        Tksright_tot= 0.0_dp
        dPdx= (uin(l,i,j,k,ipress)-uin(l,i-1,j,k,ipress))/dx
        csleft=sqrt(gamma*uin(l,i-1,j,k,ipress)/uin(l,i-1,j,k,idens))
        csright=sqrt(gamma*uin(l,i+1,j,k,ipress)/uin(l,i+1,j,k,idens))

        do idust= 1, ndust
           Tksleft_tot=Tksleft_tot+uin(l,i-1,j,k,firstindex_ndust+idust)*d_grain(idust)*l_grain(idust)/uin(l,i-1,j,k,idens)/csleft*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i-1,j,k,firstindex_ndust+idust))
           Tksright_tot=Tksright_tot+uin(l,i+1,j,k,firstindex_ndust+idust)*d_grain(idust)*l_grain(idust)/uin(l,i+1,j,k,idens)/csright*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i+1,j,k,firstindex_ndust+idust))
        end do
        !print *,Tksleft_tot , Tksright_tot
        if (no_interaction) Tksleft_tot = 0.0d0
        if (no_interaction) Tksright_tot = 0.0d0
        do idust= 1, ndust
           Tksleft(idust) =d_grain(idust)*l_grain(idust)/uin(l,i-1,j,k,idens)/csleft*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i-1,j,k,firstindex_ndust+idust))+Tksleft_tot
           Tksright(idust)=d_grain(idust)*l_grain(idust)/uin(l,i+1,j,k,idens)/csright*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i+1,j,k,firstindex_ndust+idust)) +Tksright_tot
        end do
        t_dyn = 0.5d0*(sqrt(pi/uin(l,i-1,j,k,idens))+sqrt(pi/uin(l,i,j,k,idens)))
        do idust=1,ndust
           !First order terms
           speed  = 0.5d0*(Tksright(idust)/uin(l,i,j,k,idens)+Tksleft(idust)/uin(l,i-1,j,k,idens))*dPdx
    
           fx(idust)= max(speed*uin(l,i-1,j,k,firstindex_ndust+idust)*uin(l,i-1,j,k,idens),0.0d0) +min(speed*uin(l,i,j,k,firstindex_ndust+idust)*uin(l,i,j,k,idens),0.0d0)
           !Second order terms
           if(speed.ge.0.0d0) isl = i-1
           if(speed.lt.0.0d0) isl = i
           call minmod_dust((uin(l,isl,j,k,firstindex_ndust+idust)-uin(l,isl-1,j,k,firstindex_ndust+idust))/dx,(uin(l,isl+1,j,k,firstindex_ndust+idust)-uin(l,isl,j,k,firstindex_ndust+idust))/dx,sigma)
           
           fx(idust) = fx(idust) + 0.5d0*abs(speed)*(dx-abs(speed)*dt)*sigma
        end do
        do idust= 1, ndust
           myflux(l,i,j,k,idust)=fx(idust)*dt/dx!/dx_loc
        end do
    enddo
  enddo
  enddo
  enddo
  
end subroutine dustXflx

subroutine dustYflx(uin,myflux,dx,dt,ngrid,ffdx)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none 

  integer ::ngrid
  real(dp)::dx,dt
  real(dp), dimension(1:ndust) ::d_grain,l_grain,isnot_charged

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:ndust)::myflux

  ! Primitive variables
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::ffdx

  real(dp)::dx_loc,sum_dust,Tksleft_tot,Tksright_tot,csleft,csright
  real(dp),dimension(1:ndust)::fdust, Tksleft, Tksright
  real(dp),dimension(1:ndust)::fx
  real(dp) :: speed, sigma,dPdx,scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2
  real(dp):: t_dyn,entho,pi
  integer::i,j,k,l,isl,idust, idens, ipress
  integer::jlo,jhi,klo,khi,ilo,ihi
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  entho= one /(gamma -one)
  pi =3.14159265358979323846_dp
  if(mrn.eqv..true.) then
     isnot_charged=0.0d0    
     call size_dust(l_grain)
     do idust=1,ndust
       l_grain(idust) = l_grain(idust)/scale_l
       d_grain(idust)=grain_dens(idust)/scale_d
       isnot_charged(idust)=0.0d0
    end do
  else
     do idust=1,ndust
       d_grain(idust)=grain_dens(idust)/scale_d
       l_grain(idust)=grain_size(idust)/scale_l
       isnot_charged(idust)=0.0d0

    end do
 endif
  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)
  idens=1
  ipress=5
  do k=klo,khi
  do j=jf1,jf2
  do i=ilo,ihi
     do l = 1, ngrid
        Tksleft     = 0.0_dp
        Tksright    = 0.0_dp
        Tksleft_tot = 0.0_dp
        Tksright_tot= 0.0_dp
        dPdx= (uin(l,i,j,k,ipress)-uin(l,i,j-1,k,ipress))/dx
        csleft=sqrt(gamma*uin(l,i,j-1,k,ipress)/uin(l,i,j-1,k,idens))
        csright=sqrt(gamma*uin(l,i,j+1,k,ipress)/uin(l,i,j+1,k,idens))

        do idust= 1, ndust
           Tksleft_tot=Tksleft_tot+uin(l,i,j-1,k,firstindex_ndust+idust)*d_grain(idust)*l_grain(idust)/uin(l,i,j-1,k,idens)/csleft*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j-1,k,firstindex_ndust+idust))
           Tksright_tot=Tksright_tot+uin(l,i,j+1,k,firstindex_ndust+idust)*d_grain(idust)*l_grain(idust)/uin(l,i,j+1,k,idens)/csright*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j+1,k,firstindex_ndust+idust))
        end do
        !print *,Tksleft_tot , Tksright_tot
        if (no_interaction) Tksleft_tot = 0.0d0
        if (no_interaction) Tksright_tot = 0.0d0
        do idust= 1, ndust
           Tksleft(idust) =d_grain(idust)*l_grain(idust)/uin(l,i,j-1,k,idens)/csleft*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j-1,k,firstindex_ndust+idust))+Tksleft_tot
           Tksright(idust)=d_grain(idust)*l_grain(idust)/uin(l,i,j+1,k,idens)/csright*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j+1,k,firstindex_ndust+idust)) +Tksright_tot
        end do
        t_dyn = 0.5d0*(sqrt(pi/uin(l,i-1,j,k,idens))+sqrt(pi/uin(l,i,j,k,idens)))
        do idust=1,ndust
           !First order terms
           speed  = 0.5d0*(Tksright(idust)/uin(l,i,j,k,idens)+Tksleft(idust)/uin(l,i,j-1,k,idens))*dPdx
    
           fx(idust)= max(speed*uin(l,i,j-1,k,firstindex_ndust+idust)*uin(l,i,j-1,k,idens),0.0d0) +min(speed*uin(l,i,j,k,firstindex_ndust+idust)*uin(l,i,j,k,idens),0.0d0)
           !Second order terms
           if(speed.ge.0.0d0) isl = j-1
           if(speed.lt.0.0d0) isl = j
           call minmod_dust((uin(l,i,isl,k,firstindex_ndust+idust)-uin(l,i,isl,k,firstindex_ndust+idust))/dx,(uin(l,i,isl,k,firstindex_ndust+idust)-uin(l,i,isl,k,firstindex_ndust+idust))/dx,sigma)
           
           fx(idust) = fx(idust) + 0.5d0*abs(speed)*(dx-abs(speed)*dt)*sigma
        end do
        do idust= 1, ndust
           myflux(l,i,j,k,idust)=fx(idust)*dt/dx!/dx_loc
        end do
    enddo
  enddo
  enddo
  enddo
  
end subroutine dustYflx

subroutine dustZflx(uin,myflux,dx,dt,ngrid,ffdx)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none 

  integer ::ngrid
  real(dp)::dx,dt
  real(dp), dimension(1:ndust) ::d_grain,l_grain,isnot_charged

  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:ndust)::myflux

  ! Primitive variables
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2)::ffdx

  real(dp)::dx_loc,sum_dust,Tksleft_tot,Tksright_tot,csleft,csright
  real(dp),dimension(1:ndust)::fdust, Tksleft, Tksright
  real(dp),dimension(1:ndust)::fx
  real(dp) :: speed, sigma,dPdx,scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2
  real(dp):: t_dyn,entho,pi
  integer::i,j,k,l,isl,idust, idens, ipress
  integer::jlo,jhi,klo,khi,ilo,ihi
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)
  entho= one /(gamma -one)
  pi =3.14159265358979323846_dp
  if(mrn.eqv..true.) then
     isnot_charged=0.0d0    
     call size_dust(l_grain)
     do idust=1,ndust
       l_grain(idust) = l_grain(idust)/scale_l
       d_grain(idust)=grain_dens(idust)/scale_d
       isnot_charged(idust)=0.0d0
    end do
  else
     do idust=1,ndust
       d_grain(idust)=grain_dens(idust)/scale_d
       l_grain(idust)=grain_size(idust)/scale_l
       isnot_charged(idust)=0.0d0

    end do
 endif
  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)
  idens=1
  ipress=5
  do k=kf1,kf2
  do j=jlo,jhi
  do i=ilo,ihi
     do l = 1, ngrid
        Tksleft     = 0.0_dp
        Tksright    = 0.0_dp
        Tksleft_tot = 0.0_dp
        Tksright_tot= 0.0_dp
        dPdx= (uin(l,i,j,k,ipress)-uin(l,i,j,k-1,ipress))/dx
        csleft=sqrt(gamma*uin(l,i,j,k-1,ipress)/uin(l,i,j,k-1,idens))
        csright=sqrt(gamma*uin(l,i,j,k+1,ipress)/uin(l,i,j,k+1,idens))

        do idust= 1, ndust
           Tksleft_tot=Tksleft_tot+uin(l,i,j,k-1,firstindex_ndust+idust)*d_grain(idust)*l_grain(idust)/uin(l,i,j,k-1,idens)/csleft*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j,k-1,firstindex_ndust+idust))
           Tksright_tot=Tksright_tot+uin(l,i,j,k+1,firstindex_ndust+idust)*d_grain(idust)*l_grain(idust)/uin(l,i,j,k+1,idens)/csright*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j,k+1,firstindex_ndust+idust))
        end do
        !print *,Tksleft_tot , Tksright_tot
        if (no_interaction) Tksleft_tot = 0.0d0
        if (no_interaction) Tksright_tot = 0.0d0
        do idust= 1, ndust
           Tksleft(idust) =d_grain(idust)*l_grain(idust)/uin(l,i,j,k-1,idens)/csleft*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j,k-1,firstindex_ndust+idust))+Tksleft_tot
           Tksright(idust)=d_grain(idust)*l_grain(idust)/uin(l,i,j,k+1,idens)/csright*SQRT(pi*gamma/8.0_dp)/(1.0d0-uin(l,i,j,k+1,firstindex_ndust+idust)) +Tksright_tot
        end do
        t_dyn = 0.5d0*(sqrt(pi/uin(l,i-1,j,k,idens))+sqrt(pi/uin(l,i,j,k,idens)))
        do idust=1,ndust
           !First order terms
           speed  = 0.5d0*(Tksright(idust)/uin(l,i,j,k,idens)+Tksleft(idust)/uin(l,i,j,k-1,idens))*dPdx
    
           fx(idust)= max(speed*uin(l,i,j,k-1,firstindex_ndust+idust)*uin(l,i,j,k-1,idens),0.0d0) +min(speed*uin(l,i,j,k,firstindex_ndust+idust)*uin(l,i,j,k,idens),0.0d0)
           !Second order terms
           if(speed.ge.0.0d0) isl = k-1
           if(speed.lt.0.0d0) isl = k
           call minmod_dust((uin(l,i,j,isl,firstindex_ndust+idust)-uin(l,i,j,isl,firstindex_ndust+idust))/dx,(uin(l,i,j,isl,firstindex_ndust+idust)-uin(l,i,j,isl,firstindex_ndust+idust))/dx,sigma)
           
           fx(idust) = fx(idust) + 0.5d0*abs(speed)*(dx-abs(speed)*dt)*sigma
        end do
        do idust= 1, ndust
           myflux(l,i,j,k,idust)=fx(idust)*dt/dx!/dx_loc
        end do
    enddo
  enddo
  enddo
  enddo
  
end subroutine dustZflx


subroutine minmod_dust(a,b,sigma)
  use amr_parameters
  use hydro_parameters

  implicit none
  real(dp)::a,b
  real(dp):: sigma
  if (abs(a).gt.abs(b)) sigma=b
  if (abs(b).ge.abs(a)) sigma=a
  if (a*b.le.0.0d0) sigma=0.0d0
end subroutine minmod_dust

subroutine vanleer(a,b,c,sigma)
  use amr_parameters
  use hydro_parameters

  implicit none
  real(dp)::a,b,c
  real(dp)::sigma
  real(dp)::sigma2
  call minmod_dust(a,b,sigma2)
  call minmod_dust(sigma2,c,sigma)

end subroutine vanleer
