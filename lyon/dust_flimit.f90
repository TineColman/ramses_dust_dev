! ------------------------------------------------------------------------------
!  DUSTDIFF_SPLIT  This routine solves the dust flux after the operator splitting  
!              
!
!  inputs/outputs
!  uin         => (const)  input state
!  iu1,iu2     => (const)  first and last index of input array,
!  ju1,ju2     => (const)  cell centered,    
!  ku1,ku2     => (const)  including buffer cells.
!  flux       <=  (modify) return fluxes in the 3 coord directions
!  if1,if2     => (const)  first and last index of output array,
!  jf1,jf2     => (const)  edge centered,
!  kf1,kf2     => (const)  for active cells only.
!  dx,dy,dz    => (const)  (dx,dy,dz)
!  dt          => (const)  time step
!  ngrid       => (const)  number of sub-grids
!  ndim        => (const)  number of dimensions
!
!  uin = (\rho eps_1, .... \rho eps_ndust,vdust ....,\rho,P)
! --------------------------------------------------------------------------
subroutine dustdiff_flimit(uin,flux,dx,dy,dz,dt,ngrid)
  use amr_parameters
  use const             
  use hydro_parameters
  implicit none
  
  integer ::ngrid
  real(dp)::dx,dy,dz,dt

  ! Input states
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+ndust*ndim)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+ndust*ndim,1:ndim),save::dq
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+ndust*ndim,1:ndim),save::qm
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+ndust*ndim,1:ndim),save::qp
  
  ! Output fluxes
  real(dp),dimension(1:nvector,if1:if2,jf1:jf2,kf1:kf2,1:ndust,1:ndim)::flux
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust),save::Xflux,Yflux,Zflux
  real(dp)::flxmax,ddold,dold,dxdt,r,philim,c,cl,theta
  ! Local scalar variables
  integer::i,j,k,l,ivar, idust,jdust
  integer::ilo,ihi,jlo,jhi,klo,khi

  ilo=MIN(1,iu1+2); ihi=MAX(1,iu2-2)
  jlo=MIN(1,ju1+2); jhi=MAX(1,ju2-2)
  klo=MIN(1,ku1+2); khi=MAX(1,ku2-2)

  !flux=0.0d0
  Xflux=0.0d0
  Yflux=0.0d0
  Zflux=0.0d0
  qm=0.0d0
  qp= 0.0d0
  dq=0.0d0
  dxdt=dx/dt
  ! Compute TVD slopes

  ! Save flux in output array
  do i=if1,if2
  do j=jlo,jhi
  do k=klo,khi
     do l=1,ngrid
        do idust=1,ndust
           r=(uin(l,i-1,j,k,idust)-uin(l,i-2,j,k,idust))/(uin(l,i,j,k,idust)-uin(l,i-1,j,k,idust))
           theta= sign(1.0d0,uin(l,i-1,j,k,idust+idust))
           Xflux(l,i,j,k,idust)= 0.25d0*(uin(l,i,j,k,idust+idust)+uin(l,i-1,j,k,idust+idust))*((1.0d0+theta)*uin(l,i-1,j,k,idust)+(1.0d0-theta)*uin(l,i,j,k,idust))&
                & +0.25d0*abs(uin(l,i,j,k,idust+idust)+uin(l,i-1,j,k,idust+idust))*(1.0d0-0.5*abs(uin(l,i,j,k,idust+idust)+uin(l,i-1,j,k,idust+idust))*dt/dx)*philim(r)*(uin(l,i,j,k,idust)-uin(l,i-1,j,k,idust))
           flux(l,i,j,k,idust,1)=Xflux(l,i,j,k,idust)*dt/dx
        end do
     end do
  end do
  end do
  end do

  ! Solve for 1D flux in Y direction
#if NDIM>1
 
 
  ! Save flux in output array
  do i=ilo,ihi
  do j=jf1,jf2
  do k=klo,khi
     do l=1,ngrid

        do idust=1,ndust
           r=(uin(l,i,j-1,k,idust)-uin(l,i,j-2,k,idust))/(uin(l,i,j,k,idust)-uin(l,i,j-1,k,idust))
           theta= sign(1.0d0,uin(l,i-1,j,k,idust+2*idust))
           Yflux(l,i,j,k,idust)= 0.25d0*(uin(l,i,j,k,idust+2*idust)+uin(l,i,j-1,k,idust+2*idust))*((1.0d0+theta)*uin(l,i,j-1,k,idust)+(1.0d0-theta)*uin(l,i,j,k,idust))&
                & +0.25d0*abs(uin(l,i,j,k,idust+2*idust)+uin(l,i,j-1,k,idust+2*idust))*(1.0d0-0.5*abs(uin(l,i,j,k,idust+2*idust)+uin(l,i,j-1,k,idust+2*idust))*dt/dx)*philim(r)*(uin(l,i,j,k,idust)-uin(l,i,j-1,k,idust))
           flux(l,i,j,k,idust,2)=yflux(l,i,j,k,idust)*dt/dy
        end do
     end do
  end do
  end do
  end do
#endif

  ! Solve for 1D flux in Z direction
#if NDIM>2
 
  ! Save flux in output array
  do i=ilo,ihi
  do j=jlo,jhi
  do k=kf1,kf2
     do l=1,ngrid
        do idust=1,ndust
           r=(uin(l,i,j,k-1,idust)-uin(l,i,j,k-2,idust))/(uin(l,i,j,k,idust)-uin(l,i,j,k-1,idust))
           theta= sign(1.0d0,uin(l,i,j,k-1,idust+3*idust))
           Zflux(l,i,j,k,idust)= 0.25d0*(uin(l,i,j,k,idust+3*idust)+uin(l,i,j,k-1,idust+3*idust))*((1.0d0+theta)*uin(l,i,j,k-1,idust)+(1.0d0-theta)*uin(l,i,j,k,idust))&
                & +0.25d0*abs(uin(l,i,j,k,idust+3*idust)+uin(l,i,j,k-1,idust+3*idust))*(1.0d0-0.5*abs(uin(l,i,j,k,idust+3*idust)+uin(l,i,j,k-1,idust+3*idust))*dt/dx)*philim(r)*(uin(l,i,j,k,idust)-uin(l,i,j,k-1,idust))
           flux(l,i,j,k,idust,3)=zflux(l,i,j,k,idust)*dt/dz
        end do
     end do
  end do
end do
end do
#endif
end subroutine dustdiff_flimit

function philim(r)
  use amr_parameters
  use hydro_parameters,only : dust_flimitor
    implicit none
    real(dp) ::r
    real(dp) :: philim
    select case(dust_flimitor)

    case('vanleer')
       philim= (r + abs(r))/(1.0d0+r) !VanLeer
    case('donorcell')
       philim= 0.0d0 !donor cell
    case('mc')
       philim= max(0.0d0,min(min((1.0d0+r)/2.0d0,2.0d0),2.0d0*r)) !MC
    case('va1')
       philim= (r**2.0+r)/(r**2.0+1) ! Van Albdada 1
    case('va2')
       philim=2.0d0*r/(r**2.0+1) ! Van Albdada 2
    case('minmod')
       philim=max(0.0d0,min(1.0d0,r)) !minmod
    CASE DEFAULT
    write(*,*)'unknown dust limiter'
    call clean_stop
    end select
end function philim
