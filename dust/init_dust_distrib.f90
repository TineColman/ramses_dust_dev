subroutine init_dust_ratio(dust2gasratio,epsilondust)
  use dust_parameters !size_max, size_min, mrn_index
  use hydro_parameters, only : ndust
  implicit none
  real(dp) :: dust2gasratio
  real(dp), dimension(1:ndust):: epsilondust
  !------------------------------------------------
  ! Initialize the dust
  !------------------------------------------------
  real(dp), dimension(1:ndust+1):: sdust
  real(dp) :: epsilon_0,Anorm
  integer  :: idust

  epsilon_0 = dust2gasratio/(1.0d0+dust2gasratio)
  Anorm = 1.0d0/(size_max**(4.0d0-mrn_index)-size_min**(4.0d0-mrn_index))

  do idust =1,ndust+1
     sdust(idust) = 10**(log10(size_max/size_min)*dble(idust-1)/dble(ndust)+log10(size_min))
  enddo
  do idust=1,ndust
     epsilondust(idust)= epsilon_0*Anorm*(sdust(idust+1)**(4.0d0-mrn_index)-sdust(idust)**(4.0d0-mrn_index))
  enddo
end subroutine init_dust_ratio

subroutine size_dust(sdust)
  use dust_parameters !size_max, size_min
  use hydro_parameters, only : ndust
  implicit none
  real(dp), dimension(1:ndust):: sdust
  !------------------------------------------------
  ! TODO
  !------------------------------------------------
  real(dp), dimension(1:ndust+1):: sdust_interval
  integer  :: idust

  do idust =1,ndust+1
     sdust_interval(idust) = 10**(log10(size_max/size_min)*dble(idust-1)/dble(ndust)+log10(size_min))
  enddo
  !We compute the average dust size in the bin to get the correct stopping time 
  do idust =1,ndust
     sdust(idust) = sqrt(sdust_interval(idust)*sdust_interval(idust+1))
  enddo
end subroutine size_dust


