module dust_commons
  use amr_parameters, only : dp
  real(dp),allocatable,dimension(:):: dflux_gas! Dust flux from refined interfaces for enint
  real(dp),allocatable,dimension(:):: dflux_dust_eint ! Dust flux from refined interfaces for enint
  real(dp),allocatable,dimension(:,:):: dflux_dust ! Dust flux from refined interfaces
  real(dp),allocatable,dimension(:,:,:):: v_dust ! Differential velocity of the dust phases
  integer::jdust
end module dust_commons
