module dust_parameters
  use amr_parameters, only : dp, MAXBOUND, MAXREGION
  use hydro_parameters, only : ndust


  real(dp),dimension(1:MAXBOUND,1:ndust)::dust_bound=0.01d0
  real(dp),dimension(1:MAXREGION,1:ndust)::dust2gas_region=0.01d0

  real(dp),dimension(1:ndust)::err_grad_dust=-1.0  ! Dust concentration gradient
  real(dp)::floor_dust=1.d-10   ! Dust concentration floor

  ! Dust related parameters
  integer :: grad_method=1
  logical :: K_drag = .false.
  logical :: mrn = .false.
  logical :: reduce_wdust=.false.
  logical :: reduce_tstop =.false.
  real(dp),dimension(1:ndust) :: K_dust = 1.0_dp
  real(dp),dimension(1:ndust):: grain_size = 1.0e-7_dp ! Dust properties (1:ndust) where the sizes of the grains are stocked
  real(dp),dimension(1:ndust):: grain_dens = 1.0_dp ! Dust properties (1:ndust) where the intrinsic of the grains are stocked
  real(dp),dimension(1:ndust) :: dust2gas= 0.01_dp ! To start with the usual value
  real(dp) :: size_min  = 5.0d-7
  real(dp) :: size_max  = 2.5d-5
  real(dp) :: mrn_index = 3.5d0
  integer  :: slope_dust = 7 ! 1 for upwind, 2 for minmod, 7 for vanleer
  logical  :: kwok_correction=.false. ! Apply Kwok(1975), Draine & Salpeter (1979)  correction for supersonic flows 
  logical  :: vmax_barycenter=.false. ! Set the maximum delta v to the barycenter velocity
  logical  :: vmax_cs =.false.
  logical  :: vmax_dust_lim=.false.
  real(dp) :: f_vmax=1.0d0 ! fraction of maximum velocity allowed
  real(dp) :: vdust_max=1.d5

end module dust_parameters
