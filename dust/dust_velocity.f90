subroutine set_vdust(ilevel)
  use amr_commons
  implicit none
  integer::ilevel
  !--------------------------------------------------------------------------
  ! This routine is a wrapper to the dust velocity scheme.
  ! Small grids (2x2x2) are gathered from level ilevel and sent to the
  ! hydro solver. On entry, hydro variables are gathered from array uold.
  ! On exit, vdust has been updated. 
  !--------------------------------------------------------------------------
  integer::i,igrid,ncache,n_grid
  integer,dimension(1:nvector),save::ind_grid

  if(numbtot(1,ilevel)==0)return
  if(verbose)write(*,111)ilevel

  ncache=active(ilevel)%ngrid
  do igrid=1,ncache,nvector
     n_grid=MIN(nvector,ncache-igrid+1)
     do i=1,n_grid
        ind_grid(i)=active(ilevel)%igrid(igrid+i-1)
     end do
     call vdustfine1(ind_grid,n_grid,ilevel)
  end do

111 format('   Entering set_vdust for level ',i2)

end subroutine set_vdust
!###########################################################
!###########################################################
!###########################################################
!###########################################################
subroutine vdustfine1(ind_grid,ncache,ilevel)
  use amr_commons
  use hydro_commons
  use dust_commons

  implicit none
  integer::ilevel,ncache
  integer,dimension(1:nvector)::ind_grid
  !---------------------------------------------------------------------!
  ! This routine gathers first hydro variables from neighboring grids  -!
  ! to set initial conditions in a 6x6x6 grid. It interpolate from     -!
  ! coarser level missing grid variables. 
  !---------------------------------------------------------------------!
  real(dp),dimension(1:nvector,0:twondim  ,1:nvar+3),save::u1
  real(dp),dimension(1:nvector,1:twotondim,1:nvar+3),save::u2
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3),save::uloc
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+1,1:ndim),save::vloc
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+1,1:ndim),save::vdloc

  !logical ,dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2),save::ok
  integer ,dimension(1:nvector,1:threetondim),save::nbors_father_cells
  integer ,dimension(1:nvector,1:twotondim  ),save::nbors_father_grids !dummy
  integer ,dimension(1:nvector,0:twondim    ),save::ibuffer_father
  integer ,dimension(1:nvector,0:twondim    ),save::ind1
  integer ,dimension(1:nvector),save::igrid_nbor,ind_cell,ind_buffer,ind_exist,ind_nexist

  integer::idust
  integer::i,j,ivar,idim,ind_son,ind_father,iskip,nbuffer
  integer::i0,j0,k0,i1,j1,k1,i2,j2,k2,i3,j3,k3,nexist
  integer::i1max,j1max,k1max,i2max,j2max,k2max,i3max,j3max,k3max,k1min,j1min,i1min,k2min,j2min,i2min
  real(dp)::dx,scale
  ! Mesh spacing in that level
  scale=boxlen/dble(icoarse_max-icoarse_min+1)
  dx=0.5D0**ilevel*scale

  ! Integer constants
  i1max=0; i2max=0; j1max=0; j2max=0; k1max=0; k2max=0
  j1min=0; j1max=0; j2min=0; j2max=0;
  k1min=0; k1max=0; k2min=0; k2max=0; 
  if(ndim>0)then
     i1max=2; i2max=1; i3max=2
  end if
  if(ndim>1)then
     j1max=2; j2max=1; j3max=2
  end if
  if(ndim>2)then
     k1max=2; k2max=1; k3max=2
  end if

  !------------------------------------------
  ! Gather 3^ndim neighboring father cells
  !------------------------------------------
  do i=1,ncache
     ind_cell(i)=father(ind_grid(i))
  end do
  call get3cubefather(ind_cell,nbors_father_cells,nbors_father_grids,ncache,ilevel)

  !---------------------------
  ! Gather 6x6x6 cells stencil
  !---------------------------
  ! Loop over 3x3x3 neighboring father cells
  do k1=k1min,k1max
  do j1=j1min,j1max
  do i1=i1min,i1max

     ! Check if neighboring grid exists
     nbuffer=0
     nexist=0
     ind_father=1+i1+3*j1+9*k1
     do i=1,ncache
        igrid_nbor(i)=son(nbors_father_cells(i,ind_father))
        if(igrid_nbor(i)>0) then
           nexist=nexist+1
           ind_exist(nexist)=i
        else
          nbuffer=nbuffer+1
          ind_nexist(nbuffer)=i
          ind_buffer(nbuffer)=nbors_father_cells(i,ind_father)
        end if
     end do

     ! If not, interpolate variables from parent cells
     if(nbuffer>0)then
        call getnborfather(ind_buffer,ibuffer_father,nbuffer,ilevel)
        do j=0,twondim
           do ivar=1,nvar+3
              do i=1,nbuffer
                 u1(i,j,ivar)=uold(ibuffer_father(i,j),ivar)
              end do
           end do
           do i=1,nbuffer
              ind1(i,j)=son(ibuffer_father(i,j))
           end do
        end do
        call interpol_hydro(u1,ind1,u2,nbuffer)
     endif
     ! Loop over 2x2x2 cells
     do k2=k2min,k2max
     do j2=j2min,j2max
     do i2=i2min,i2max

        ind_son=1+i2+2*j2+4*k2
        iskip=ncoarse+(ind_son-1)*ngridmax
        do i=1,nexist
           ind_cell(i)=iskip+igrid_nbor(ind_exist(i))
        end do

        i3=1; j3=1; k3=1
        if(ndim>0)i3=1+2*(i1-1)+i2
        if(ndim>1)j3=1+2*(j1-1)+j2
        if(ndim>2)k3=1+2*(k1-1)+k2

        ! Gather hydro variables
        do ivar=1,nvar+3
           do i=1,nexist
              uloc(ind_exist(i),i3,j3,k3,ivar)=uold(ind_cell(i),ivar)
           end do
           do i=1,nbuffer
              uloc(ind_nexist(i),i3,j3,k3,ivar)=u2(i,ind_son,ivar)
           end do
        end do

        ! Gather dust velocity
        do idust=1,ndust+1
           do idim=1,ndim
              do i=1,nexist
                 vdloc(ind_exist(i),i3,j3,k3,idust,idim)=v_dust(ind_cell(i),idust,idim)
              end do
              do i=1,nbuffer
                 vdloc(ind_nexist(i),i3,j3,k3,idust,idim)=v_dust(ibuffer_father(i,0),idust,idim)
              end do
           end do
        end do

     end do
     end do
     end do
     ! End loop over cells

  end do
  end do
  end do
  ! End loop over neighboring grids

  call cmpvdust(uloc,vloc,vdloc,dx,dx,dx,ncache)

  !--------------------------------------------------------
  !Udate at level ilevel for the dust velocity
  !--------------------------------------------------------
  do idim=1,ndim
     i0=0; j0=0; k0=0
     if(idim==1)i0=1
     if(idim==2)j0=1
     if(idim==3)k0=1   
     do k2=k2min,k2max
     do j2=j2min,j2max
     do i2=i2min,i2max
        ind_son=1+i2+2*j2+4*k2
        iskip=ncoarse+(ind_son-1)*ngridmax
        do i=1,ncache
           ind_cell(i)=iskip+ind_grid(i)
        end do
        i3=1+i2
        j3=1+j2
        k3=1+k2
        do i=1,ncache
           if(son(ind_cell(i))==0)then
              do idust=1,ndust+1
                 v_dust(ind_cell(i),idust,idim)=vloc(i,i3,j3,k3,idust,idim)
              enddo
           end if
        end do
     end do
     end do
     end do
  end do

end subroutine vdustfine1

!###########################################################
!###########################################################
!###########################################################
!###########################################################

subroutine cmpvdust(uin,vout,vdin,dx,dy,dz,ngrid)
  use amr_parameters
  use hydro_commons
  use dust_parameters
  implicit none
  integer ::ngrid
  real(dp)::dx, dy, dz
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+1,1:ndim)::vout
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:ndust+1,1:ndim)::vdin
  !---------------------
  ! TODO description
  !--------------------
  ! Primitive variables
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar)::qin

  ! declare local variables
  integer ::i, j, k, l
  integer ::ilo,ihi,jlo,jhi,klo,khi
  integer ::idust,idim
  real(dp) :: d,u,v,w,e_mag,e_kin, sum_dust, enint
  real(dp) :: cs,wnorm,vmax, Mach_dv
  real(dp),dimension(1:ndim) :: fpress,fmag,grad_P
  real(dp),dimension(1:ndust)  :: t_stop
  real(dp)  ::pi,tstop_tot,dens_floor
  real(dp), dimension(1:ndust) ::d_grain,l_grain
  real(dp),dimension(1:ndim):: ew
  real(dp)::scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2
#ifdef SOLVERmhd
  real(dp) ::A,B,C,dAy, dAz,dBx,dBz,dCx,dCy
#endif
  call units(scale_l,scale_t,scale_d,scale_v,scale_nH,scale_T2)

  ilo=MIN(1,iu1+1); ihi=MAX(1,iu2-1)
  jlo=MIN(1,ju1+1); jhi=MAX(1,ju2-1)
  klo=MIN(1,ku1+1); khi=MAX(1,ku2-1)

  vmax=vdust_max/scale_v
  pi =3.14159265358979323846_dp
  e_mag=0.0d0
  if(mrn.eqv..true.) then
     call size_dust(l_grain)
     do idust=1,ndust
       l_grain(idust) = l_grain(idust)/scale_l
       d_grain(idust)=grain_dens(idust)/scale_d
    end do
  else
     do idust=1,ndust
       d_grain(idust)=grain_dens(idust)/scale_d
       l_grain(idust)=grain_size(idust)/scale_l

    end do
  endif
  call ctoprimdust(uin,qin,ngrid)
  do k = klo, khi
     do j = jlo, jhi
        do i = ilo, ihi
           do l = 1, ngrid
              d= max(qin(l,i,j,k,1),smallr)
                 tstop_tot=0.0d0
                 t_stop=0.0d0
                 sum_dust=0.0d0
                 do idust = 1,ndust
                    sum_dust= sum_dust + qin(l,i,j,k,firstindex_ndust+idust)
                 end do
#ifdef SOLVERmhd
                 !magnetic field and required derivatives to get rotB
                 A=0.5d0*(uin(l,i,j,k,6)+uin(l,i,j,k,nvar+1))
                 B=0.5d0*(uin(l,i,j,k,7)+uin(l,i,j,k,nvar+2))                 
                 C=0.5d0*(uin(l,i,j,k,8)+uin(l,i,j,k,nvar+3))
                 e_mag=0.5d0*(A**2+B**2+C**2)
#endif

                 u=0.0d0; v=0.0d0; w=0.0d0

                 u = qin(l,i,j,k,2)
                 if(ndim>1)v = qin(l,i,j,k,3)
                 if(ndim>2)w = qin(l,i,j,k,4)
                 e_kin=0.5d0*d*(u**2+v**2+w**2)
                 e_mag=0.0d0
#if NENER>0
                 do irad=1,nener
                    e_mag=e_mag+uin(l,i,j,k,8+irad) !TODO index?
                 end do
#endif
                 enint=uin(l,i,j,k,index_P)-e_kin-e_mag
                 cs = sqrt(gamma*(gamma-1.0d0)*enint/((1.0_dp-sum_dust)*d))

                 do idust = 1,ndust
                    t_stop(idust) =  d_grain(idust)*l_grain(idust)*SQRT(pi*gamma/8.0_dp)/cs/(d- uin(l,i,j,k,firstindex_ndust+idust))

                    if(K_drag)  t_stop(idust) = uin(l,i,j,k,firstindex_ndust+idust)/K_dust(idust)

                    tstop_tot= tstop_tot-t_stop(idust)*qin(l,i,j,k,firstindex_ndust+idust)

                    if(kwok_correction)then
#if NDIM==1
                       wnorm= sqrt(vdin(l,i,j,k,idust,1)**2.0)
#endif
#if NDIM==2
                       wnorm= sqrt(vdin(l,i,j,k,idust,1)**2.0+vdin(l,i,j,k,idust,2)**2.0)
#endif
#if NDIM==3
                       wnorm= sqrt(vdin(l,i,j,k,idust,1)**2.0+vdin(l,i,j,k,idust,2)**2.0+vdin(l,i,j,k,idust,3)**2.0)
#endif                       

                       Mach_dv = wnorm / cs
                       t_stop(idust) =  t_stop(idust) /(1.0d0+(9.0d0*pi*Mach_dv**2/128.0d0))**0.5
                    end if
                 end do
                 
                 !pressure force
                 if (grad_method==1) then !2nd order estimate
                    grad_P(1)=-(qin(l,i+1,j,k,index_P)-qin(l,i-1,j,k,index_P))*0.5d0/dx
#if NDIM>1
                    grad_P(2)=-(qin(l,i,j+1,k,index_P)-qin(l,i,j-1,k,index_P))*0.5d0/dy
#endif
#if NDIM>2                   
                    grad_P(3)=-(qin(l,i,j,k+1,index_P)-qin(l,i,j,k-1,index_P))*0.5d0/dz
#endif

#ifdef SOLVERmhd
                    dAy=(0.5d0*(uin(l,i,j+1,k,6)+uin(l,i,j+1,k,nvar+1))-0.5d0*(uin(l,i,j-1,k,6)+uin(l,i,j-1,k,nvar+1)))*0.5d0/dy
                    dAz=(0.5d0*(uin(l,i,j,k+1,6)+uin(l,i,j,k+1,nvar+1))-0.5d0*(uin(l,i,j,k-1,6)+uin(l,i,j,k-1,nvar+1)))*0.5d0/dz
                    dBx=(0.5d0*(uin(l,i+1,j,k,7)+uin(l,i+1,j,k,nvar+2))-0.5d0*(uin(l,i-1,j,k,7)+uin(l,i-1,j,k,nvar+2)))*0.5d0/dx
                    dBz=(0.5d0*(uin(l,i,j,k+1,7)+uin(l,i,j,k+1,nvar+2))-0.5d0*(uin(l,i,j,k-1,7)+uin(l,i,j,k-1,nvar+2)))*0.5d0/dz
                    dCy=(0.5d0*(uin(l,i,j+1,k,8)+uin(l,i,j+1,k,nvar+3))-0.5d0*(uin(l,i,j-1,k,8)+uin(l,i,j-1,k,nvar+3)))*0.5d0/dy
                    dCx=(0.5d0*(uin(l,i+1,j,k,8)+uin(l,i+1,j,k,nvar+3))-0.5d0*(uin(l,i-1,j,k,8)+uin(l,i-1,j,k,nvar+3)))*0.5d0/dx
#endif

                 else if (grad_method==2) then !third order estimate
                    grad_P(1)=-(-qin(l,i+2,j,k,index_P)+8.0d0*qin(l,i+1,j,k,index_P)-8.0d0*qin(l,i-1,j,k,index_P)+qin(l,i-2,j,k,index_P))/(12.0d0*dx)
#if NDIM>1
                    grad_P(2)=-(-qin(l,i,j+2,k,index_P)+8.0d0*qin(l,i,j+1,k,index_P)-8.0d0*qin(l,i,j-1,k,index_P)+qin(l,i,j-2,k,index_P))/(12.0d0*dx)
#endif
#if NDIM>2  
                    grad_P(3)=-(-qin(l,i,j,k+2,index_P)+8.0d0*qin(l,i,j,k+1,index_P)-8.0d0*qin(l,i,j,k-1,index_P)+qin(l,i,j,k-2,index_P))/(12.0d0*dx)
#endif

#ifdef SOLVERmhd
                    dAy=(0.5d0*(uin(l,i,j+1,k,6)+uin(l,i,j+1,k,nvar+1))-0.5d0*(uin(l,i,j-1,k,6)+uin(l,i,j-1,k,nvar+1)))/(12.0d0*dx)
                    dAz=(0.5d0*(uin(l,i,j,k+1,6)+uin(l,i,j,k+1,nvar+1))-0.5d0*(uin(l,i,j,k-1,6)+uin(l,i,j,k-1,nvar+1)))/(12.0d0*dx)
                    dBx=(0.5d0*(uin(l,i+1,j,k,7)+uin(l,i+1,j,k,nvar+2))-0.5d0*(uin(l,i-1,j,k,7)+uin(l,i-1,j,k,nvar+2)))/(12.0d0*dx)
                    dBz=(0.5d0*(uin(l,i,j,k+1,7)+uin(l,i,j,k+1,nvar+2))-0.5d0*(uin(l,i,j,k-1,7)+uin(l,i,j,k-1,nvar+2)))/(12.0d0*dx)
                    dCy=(0.5d0*(uin(l,i,j+1,k,8)+uin(l,i,j+1,k,nvar+3))-0.5d0*(uin(l,i,j-1,k,8)+uin(l,i,j-1,k,nvar+3)))/(12.0d0*dx)
                    dCx=(0.5d0*(uin(l,i+1,j,k,8)+uin(l,i+1,j,k,nvar+3))-0.5d0*(uin(l,i-1,j,k,8)+uin(l,i-1,j,k,nvar+3)))/(12.0d0*dx)
#endif
                 endif 
             
                 fpress= grad_P/(d*(1.0d0-sum_dust))

                 !magnetic force needed to 'make' grains neutral
#ifdef SOLVERmhd
                 fmag(1)=((dAz-dCx)*C-(dBx-dAy)*B)/(d*(1.0d0-sum_dust))
#if NDIM>1
                 fmag(2)=((dBx-dAy)*A-(dCy-dBz)*C)/(d*(1.0d0-sum_dust))
#endif
#if NDIM>2
                 fmag(3)=((dCy-dBz)*B-(dAz-dCx)*A)/(d*(1.0d0-sum_dust))
#endif
#endif
                 do idust=1,ndust
                    t_stop(idust) = t_stop(idust)+tstop_tot !cumulative back-reaction is added
                 end do
                 
                 do idust = 1,ndust
                    do idim=1,ndim
#ifdef SOLVERmhd                       
                       vout(l,i,j,k,idust,idim)= t_stop(idust)*(1.0d0-sum_dust)*(-fpress(idim)-fmag(idim))
#else
                       vout(l,i,j,k,idust,idim)= t_stop(idust)*(1.0d0-sum_dust)*(-fpress(idim))
                       
#endif                      
                    end do
                 end do
           
                 do idust=1,ndust

#if NDIM==1
                 wnorm= sqrt(vout(l,i,j,k,idust,1)**2.0)
#endif
#if NDIM==2
                 wnorm= sqrt(vout(l,i,j,k,idust,1)**2.0+vout(l,i,j,k,idust,2)**2.0)
                 
#endif
#if NDIM==3
                 wnorm= sqrt(vout(l,i,j,k,idust,1)**2.0+vout(l,i,j,k,idust,2)**2.0+vout(l,i,j,k,idust,3)**2.0)                
#endif
           ! Regularisation of the dust velocity for regions where its wrong      
           if (reduce_wdust) then   
              do idim=1,ndim
                 if(vmax_barycenter)then
                    vmax = f_vmax*sqrt(u*u+v*v+w*w)
                 end if
                 if(vmax_cs)then
                    vmax = f_vmax*cs
                 end if
                 if(vmax_dust_lim)then
                    vmax = vdust_max/scale_l*scale_t
                 end if                   
                 if(wnorm>vmax) ew(idim)= vout(l,i,j,k,idust,idim)/wnorm        
                 if(wnorm>vmax) vout(l,i,j,k,idust,idim)= ew(idim)*min(wnorm,vmax)
              end do
           end if
        end do
        do idim=1,ndim
              vout(l,i,j,k,ndust+1,idim)=- sum(vout(l,i,j,k,1:ndust,idim)*qin(l,i,j,k,1:ndust))/(1.0d0-sum_dust)
        end do
        
     end do
  end do
end do
end do

end subroutine cmpvdust

subroutine ctoprimdust(uin,q,ngrid)
  use amr_parameters
  use hydro_parameters
  use const
  implicit none

  integer ::ngrid
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar+3)::uin
  real(dp),dimension(1:nvector,iu1:iu2,ju1:ju2,ku1:ku2,1:nvar)::q  
  !---------------------
  ! TODO description
  !---------------------
  integer ::i, j, k, l
  real(dp)::eint, smalle, smallp, etot
  real(dp),dimension(1:nvector),save::eken,emag,erad

#if NENER>0
  integer::irad
#endif
  integer::n

  real(dp):: sum_dust
#if NDUST>0
  integer:: idust
#endif  
  
  smalle = smallc**2/gamma/(gamma-one)
  smallp = smallr*smallc**2/gamma

  ! Convert to primitive variable
  do k = ku1, ku2
     do j = ju1, ju2
        do i = iu1, iu2

           ! Compute density
           do l = 1, ngrid
              q(l,i,j,k,1) = max(uin(l,i,j,k,1),smallr)
           end do
           ! Debug
           if(debug)then
              do l = 1, ngrid
                 if(uin(l,i,j,k,1).le.smallr)then
                    write(*,*)'negative density'
                    write(*,*)uin(l,i,j,k,1)
                    stop
                 end if
              end do
           end if

           ! Compute velocities and cell centered magnetic field
           do l = 1, ngrid
#ifdef SOLVERmhd
              q(l,i,j,k,2) = uin(l,i,j,k,2)/q(l,i,j,k,1)
              q(l,i,j,k,3) = uin(l,i,j,k,3)/q(l,i,j,k,1)
              q(l,i,j,k,4) = uin(l,i,j,k,4)/q(l,i,j,k,1)
              q(l,i,j,k,6) = (uin(l,i,j,k,6)+uin(l,i,j,k,nvar+1))*half
              q(l,i,j,k,7) = (uin(l,i,j,k,7)+uin(l,i,j,k,nvar+2))*half
              q(l,i,j,k,8) = (uin(l,i,j,k,8)+uin(l,i,j,k,nvar+3))*half
#else
              q(l,i,j,k,2) = uin(l,i,j,k,2)/q(l,i,j,k,1)
              if(ndim>1) q(l,i,j,k,3) = uin(l,i,j,k,3)/q(l,i,j,k,1)
              if(ndim>2) q(l,i,j,k,4) = uin(l,i,j,k,4)/q(l,i,j,k,1)
#endif
           end do

           ! Compute specific kinetic energy and magnetic energy
           ! TODO not valid for 1D/2D hydro !!!
           do l = 1, ngrid
              eken(l) = half*(q(l,i,j,k,2)**2+q(l,i,j,k,3)**2+q(l,i,j,k,4)**2)
              emag(l)=0.0d0
#ifdef SOLVERmhd
              emag(l) = half*(q(l,i,j,k,6)**2+q(l,i,j,k,7)**2+q(l,i,j,k,8)**2)
#endif
           end do

           ! Compute non-thermal pressure
           erad = zero
#if NENER>0
           do irad = 1,nent
              do l = 1, ngrid
                 q(l,i,j,k,8+irad) = (gamma_rad(irad)-one)*uin(l,i,j,k,8+irad)
                 erad(l) = erad(l)+uin(l,i,j,k,8+irad)
              end do
           enddo
           do irad = 1,ngrp
              do l = 1, ngrid
                 q(l,i,j,k,firstindex_er+irad) = uin(l,i,j,k,firstindex_er+irad)
                 erad(l) = erad(l)+uin(l,i,j,k,firstindex_er+irad)
              end do
           enddo
#endif

           ! Compute thermal pressure through EOS
           do l = 1, ngrid
              
              etot = uin(l,i,j,k,index_P) - emag(l) -erad(l)
              eint = etot-eken(l)*q(l,i,j,k,1)            
              q(l,i,j,k,index_P)=MAX((gamma-1.0d0)*eint,smallp)
           end do

        end do
     end do
  end do
!dust  
  do n = firstindex_ndust, firstindex_ndust+ndust
     do k = ku1, ku2
        do j = ju1, ju2
           do i = iu1, iu2
              do l = 1, ngrid
                 q(l,i,j,k,n) = uin(l,i,j,k,n)/max(uin(l,i,j,k,1),smallr)
              end do
           end do
        end do
     end do
  end do
 
end subroutine ctoprimdust

